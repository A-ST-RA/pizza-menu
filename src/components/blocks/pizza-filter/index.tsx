import { clsx } from 'clsx';
import React, { FC } from 'react';

import { PizzaIconMatcher } from '@/components/UI/svg/icons-for-pizza';
import Title from '@/components/UI/Title';
import { indexOfChoose } from '@/types/pizzaTagIdx';

import cn from './style.module.sass';

interface IPizzaFilterProps {
    tags: string[];
    activeId: indexOfChoose;
    setActive: (idx: indexOfChoose) => void;
}

const data = {
    0: 'Все',
    1: 'Острые',
    2: 'Мясные',
    3: 'Сырные',
    4: 'Веганские',
};

export const PizzaFilter: FC<IPizzaFilterProps> = ({ tags, activeId, setActive }) => (
    <div className={cn['pizza-filter']} id="shop">
        <Title customClass={cn['pizza-filter__title']} title="Выберите пиццу" level={1} />
        <div className={cn['pizza-filter__grid']}>
            {tags.map((item, idx: number) => {
                const FilterIcon = PizzaIconMatcher[idx as indexOfChoose];
                return (
                    <div
                        role="presentation"
                        key={idx.toString()}
                        className={cn['pizza-filter__item-content']}
                        onClick={() => setActive(idx as indexOfChoose)}
                    >
                        <div className={clsx(cn['pizza-filter__item'])}>
                            <div
                                className={clsx(
                                    cn['pizza-filter__item-icon'],
                                    activeId === idx && cn['pizza-filter__item-icon_active']
                                )}
                            >
                                <FilterIcon />
                            </div>
                            <span
                                className={clsx(
                                    cn['pizza-filter__item-text'],
                                    idx === activeId && cn['pizza-filter__item-text_active']
                                )}
                            >
                                {data[idx as indexOfChoose]}
                            </span>
                        </div>
                    </div>
                );
            })}
        </div>
    </div>
);
