import React from 'react';

import Title from '@/components/UI/Title';

import cn from './style.module.sass';

const Contact = (): JSX.Element => (
    <div className={cn['find-us']} id="find-us">
        <div className={cn['find-us__wrapper']}>
            <Title
                customClass={cn['find-us__title']}
                level={3}
                title="Следите за нами в Instagram"
            />
            <a href="#" target="_blank" className={cn['find-us__inst']}>
                @pizzas
            </a>
        </div>
    </div>
);

export default Contact;
