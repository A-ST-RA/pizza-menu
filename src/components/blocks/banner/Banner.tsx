import Image from 'next/image';
import React from 'react';

import Button from '@/components/UI/Button';
import Text from '@/components/UI/Text';
import Title from '@/components/UI/Title';

import cn from './style.module.sass';

const Banner = () => (
    <div className={cn.banner} id="banner">
        <div className={cn['banner__image-wrap']}>
            <div className={cn.banner__image}>
                <Image width={1007} height={630} src="/images/banner/1.png" alt="pizza" />
            </div>
        </div>
        <div className={cn.banner__wrapper}>
            <div className={cn['banner__content-inner']}>
                <Title customClass={cn.banner__title} title="Пицца на заказ" level={1} />
                <Text customClass={cn.banner__text} type="accent">
                    Бесплатная и быстрая доставка за час в любое удобное для вас время
                </Text>
                <Button buttonType="link" hrefLink="#shop" customClass={cn.banner__button}>
                    <span className="banner__button-content">Выбрать пиццу</span>
                </Button>
            </div>
        </div>
    </div>
);

export default Banner;
