import Image from 'next/image';
import React, { FC } from 'react';

import Text from '@/components/UI/Text';
import Title from '@/components/UI/Title';

import cn from './style.module.sass';

interface CardProps {
    img: string;
    title: string;
    text: string;
}

const SuggestionCard: FC<CardProps> = ({ img, title, text }): JSX.Element => (
    <div className={cn.card}>
        <Image
            src={img}
            width={416}
            height={247}
            layout="responsive"
            alt="..."
            className={cn.img}
        />
        <Title customClass={cn.card__title} level={4} title={title} />
        <Text type="paragraph" customClass={cn.text}>
            {text}
        </Text>
    </div>
);

export default SuggestionCard;
