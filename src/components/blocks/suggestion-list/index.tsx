import React, { FC } from 'react';
import Slider from 'react-slick';

import cn from './style.module.sass';
import SuggestionCard from './suggestion-card';

interface CardProps {
    id: number;
    img: string;
    title: string;
    text: string;
}

const data: CardProps[] = [
    {
        id: 1,
        img: '/images/suggestion/1.png',
        title: 'Закажи 2 пиццы – 3-я в подарок',
        text: 'При заказе 2-х больших пицц – средняя пицца в подарок',
    },
    {
        id: 2,
        img: '/images/suggestion/2.png',
        title: 'Напиток в подарок',
        text: 'Скидка на заказ от 3 000 рублей + напиток в подарок',
    },
    {
        id: 3,
        img: '/images/suggestion/3.png',
        title: '25% при первом заказе',
        text: 'Скидка новым клиентам!',
    },
];

const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 3,
    adaptiveHeight: true,
    dotsClass: 'slick-dots',
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: true,
            },
        },
        {
            breakpoint: 767,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                initialSlide: 2,
            },
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
                dots: true,
            },
        },
    ],
};

const SuggestionList: FC = (): JSX.Element => (
    <div className={cn['suggestion-list']}>
        <div className={cn['suggestion-list__wrapper']}>
            <div className={cn['suggestion-list__box']}>
                <Slider {...settings}>
                    {data.map(
                        ({ id, ...props }: CardProps): JSX.Element => (
                            <SuggestionCard key={id} {...props} />
                        )
                    )}
                </Slider>
            </div>
        </div>
    </div>
);

export default SuggestionList;
