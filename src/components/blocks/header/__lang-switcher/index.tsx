import Link from 'next/link';
import React from 'react';

import cn from './style.module.sass';

const LangSwitcher = () => (
    <div className={cn['header__langs-switcher']}>
        <Link href="#">
            <a className={cn['header__langs-tag']}>En</a>
        </Link>
    </div>
);

export default LangSwitcher;
