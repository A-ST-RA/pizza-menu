/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import clsx from 'clsx';
import React, { FC, useState } from 'react';

import Modal from '@/components/UI/Modal';
import { CartIco } from '@/components/UI/svg/cartIco';
import Text from '@/components/UI/Text';

import cn from './style.module.sass';
import MakeOrderForm from '../../forms/OrderForm/MakeOrderForm';

interface IProps {
    cartCustomClass?: string;
    iconCustomClass?: string;
}

const Cart: FC<IProps> = ({ iconCustomClass, cartCustomClass }): JSX.Element => {
    const [isOpen, setIsOpen] = useState<boolean>(false);

    return (
        <div className={cn.cart}>
            <div className={cn.cart__logo} onClick={() => setIsOpen(true)}>
                <CartIco className={clsx(iconCustomClass)} />
                <div className={cn.cart__goods}>
                    <span>{3}</span>
                </div>
            </div>
            <div className={cn['cart__text-content']}>
                <div
                    role="presentation"
                    className={clsx(cn.cart__title, cartCustomClass)}
                    onClick={() => setIsOpen(true)}
                >
                    Ваш заказ
                </div>
                <Text customClass={cn.cart__text} type="description">
                    Итальянская и ещё 2 пиццы
                </Text>
            </div>
            <MakeOrderForm isOpen={isOpen} setIsOpen={setIsOpen} />
        </div>
    );
};

export default Cart;
