import clsx from 'clsx';
import React, { FC } from 'react';

import Burger from '../../../UI/Burger';
import Menu from '../__menu';
import cn from './style.module.sass';

interface IProps {
    onToggle: (isOpen: boolean) => void;
    isOpen: boolean;
}

const MobileMenu: FC<IProps> = ({ onToggle, isOpen }) => (
    <div className={clsx(cn['mobile-menu'], !isOpen && cn['mobile-menu__hidden'])}>
        <div className={cn['mobile-menu__wrapper']}>
            <div className={cn['mobile-menu__header']}>
                <Burger onToggle={onToggle} isStateMenu={isOpen} />
            </div>
        </div>
        <div className={cn['mobile-menu__items']}>
            <Menu isMobile />
        </div>
        <div className={cn['mobile-menu__contacts']}>
            <div className={cn['mobile-menu__title']}>заказать по телефону</div>
            <div className={cn['mobile-menu__phone']}>
                <a href="tel:+79184326587">+7 (918) 432-65-87</a>
            </div>
            <div className={cn['mobile-menu__working-time']}>Ежедневно с 9:00 до 23:00</div>
        </div>
        <div className={cn['mobile-menu__langs']}>
            <a href="#">English</a>
        </div>
    </div>
);

export default MobileMenu;
