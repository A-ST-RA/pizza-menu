/* eslint-disable react/jsx-no-undef */
import clsx from 'clsx';
import React, { FC, useState } from 'react';

import Burger from '@/components/UI/Burger';
import Logo from '@/components/UI/Logo';

import Contact from './__contact';
import LangSwitcher from './__lang-switcher';
import Menu from './__menu';
import Cart from './cart';
import MobileMenu from './mobile-menu';
import cn from './style.module.sass';

const Header: FC = () => {
    const [burgerOpen, setBurgerOpen] = useState<boolean>(false);

    return (
        <div className={clsx(cn.header)}>
            <div className={cn.header__wrapper}>
                <div className={cn.header__grid}>
                    <div className={cn.header__item}>
                        <Logo />
                    </div>
                    <div className={cn.header__item}>
                        <Menu />
                    </div>
                    <div className={cn.header__item}>
                        <div className={cn.header__contact}>
                            <Contact />
                        </div>
                        <div className={cn.header__cart}>
                            <Cart />
                        </div>
                        <div className={cn.header__lang}>
                            <LangSwitcher />
                        </div>
                        <div className={cn.header__burger}>
                            <Burger onToggle={() => setBurgerOpen(true)} isStateMenu={burgerOpen} />
                        </div>
                    </div>
                </div>
            </div>
            <MobileMenu isOpen={burgerOpen} onToggle={() => setBurgerOpen(false)} />
        </div>
    );
};

export default Header;
