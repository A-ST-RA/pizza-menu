import clsx from 'clsx';
import Link from 'next/link';
import React, { FC } from 'react';

import cn from './style.module.sass';

interface MenuProps {
    menuLinkClass?: string;
    isMobile?: boolean;
}

const Menu: FC<MenuProps> = ({ menuLinkClass, isMobile = false }): JSX.Element => (
    <>
        <div className={clsx(isMobile ? cn['header__menu-mobile'] : cn.header__menu)}>
            <Link href="#shop">
                <a className={clsx(cn['header__menu-link'], menuLinkClass)}>Меню</a>
            </Link>
        </div>
        <div className={clsx(isMobile ? cn['header__menu-mobile'] : cn.header__menu)}>
            <Link href="#about">
                <a className={clsx(cn['header__menu-link'], menuLinkClass)}>О нас</a>
            </Link>
        </div>
        <div className={clsx(isMobile ? cn['header__menu-mobile'] : cn.header__menu)}>
            <Link href="#contact">
                <a className={clsx(cn['header__menu-link'], menuLinkClass)}>Контакты</a>
            </Link>
        </div>
    </>
);

export default Menu;
