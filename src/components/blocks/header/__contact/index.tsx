import clsx from 'clsx';
import React, { FC } from 'react';

import Telephone from '@/components/UI/svg/Telephone';

import cn from './style.module.sass';

interface IProps {
    contactCustomClass?: string;
    iconCustomClass?: string;
}

const Contact: FC<IProps> = ({ contactCustomClass, iconCustomClass }) => (
    <div className={cn.header__contact}>
        <div className={cn['header__contact-phone-link']}>
            <a className={cn['header__contact-phone-icon']} href="tel:+79184326587">
                <Telephone customClass={clsx(iconCustomClass)} />
            </a>
        </div>
        <div className={cn['header__contact-data']}>
            <div>
                <a
                    href="tel:+79184326587"
                    className={clsx(cn['header__contact-phone'], contactCustomClass)}
                >
                    +7 (918) 432-65-87
                </a>
            </div>
            <div className={cn['header__contact-time']}>Ежедневно с 9:00 до 23:00</div>
        </div>
    </div>
);

export default Contact;
