import clsx from 'clsx';
import Image from 'next/image';
import React, { FC } from 'react';

import cn from '../style.module.sass';

interface IPizzaCardSizeAndImageProps {
    pizzaImageUrl: string;
    pizzaName: string;
    size: 0 | 1 | 2;
}

const matcher = {
    0: 's',
    1: 'm',
    2: 'l',
};

export const PizzaCardSizeAndImage: FC<IPizzaCardSizeAndImageProps> = ({
    pizzaImageUrl,
    pizzaName,
    size,
}) => (
    <div className={clsx(cn['pizza-card__size'], cn[`pizza-card__size_large`])}>
        <div className={clsx(cn['pizza-card__size'], cn['pizza-card__size_medium'])}>
            <div className={clsx(cn['pizza-card__size'], cn['pizza-card__size_small'])}>
                <div
                    className={clsx(
                        cn['pizza-card__image'],
                        cn[`pizza-card__image_${matcher[size]}`]
                    )}
                >
                    <Image layout="fill" src={pizzaImageUrl} alt={pizzaName} />
                </div>
            </div>
        </div>
    </div>
);
