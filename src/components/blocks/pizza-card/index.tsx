import clsx from 'clsx';
import React, { FC, useState } from 'react';

import Button from '@/components/UI/Button';
import Counter from '@/components/UI/Counter';
import { PizzaIconMatcher } from '@/components/UI/svg/icons-for-pizza';
import Text from '@/components/UI/Text';
import Title from '@/components/UI/Title';
import { indexOfChoose } from '@/types/pizzaTagIdx';

import PizzaCardDetails from './__details';
import PiazzaCardPurchase from './__purchase';
import { PizzaCardSizeAndImage } from './__size/pizza-card__size';
import cn from './style.module.sass';

export interface IPizzaCardProps {
    tags: number[];
    description: string;
    title: string;
    pizzaPrices: number[];
    isInCart?: boolean;
}

const matcher = {
    0: '20cm',
    1: '30cm',
    2: '40cm',
};

const PizzaCard: FC<IPizzaCardProps> = ({
    tags,
    description,
    title,
    pizzaPrices,
    isInCart = false,
}) => {
    const [pizzaIndex, setPizzaIndex] = useState<0 | 1 | 2>(0);

    const updatePizza = (index: 0 | 1 | 2) => () => {
        setPizzaIndex(index);
    };

    return (
        <div className={clsx(cn['pizza-card'], isInCart && cn['pizza-card_in-cart'])}>
            <div className={cn['pizza-card__tags']}>
                {tags.map((el, idx) => {
                    const PizzaIcoTag = PizzaIconMatcher[el as indexOfChoose];

                    return <PizzaIcoTag key={idx} />;
                })}
            </div>
            <PizzaCardSizeAndImage
                pizzaImageUrl="/images/pizza/pizza.png"
                pizzaName={title}
                size={pizzaIndex}
            />
            {isInCart && (
                <div className={cn['pizza-card__cart-data']}>
                    <div className={cn['pizza-card__cart-data_block']}>
                        <Title customClass={cn['pizza-card__title']} title={title} level={4} />
                        <Text customClass={cn['pizza-card__ingredients']} type="description">
                            {matcher[pizzaIndex]}
                        </Text>
                    </div>
                    <div className={cn['pizza-card__cart-data_block']}>
                        <Counter count={0} inc={console.log} dec={console.log} />
                    </div>
                    <div className={cn['pizza-card__cart-data_block']}>
                        <Title
                            customClass={clsx(
                                cn['pizza-card__title'],
                                cn['pizza-card__title_padding']
                            )}
                            title={`${pizzaPrices[0]} руб.`}
                            level={3}
                        />
                    </div>
                </div>
            )}
            {!isInCart && (
                <div>
                    <PizzaCardDetails title={title} description={description} />
                    <div className={clsx(cn['pizza-diameter'], cn['pizza-diameter_wrap'])}>
                        <Text type="description" customClass={cn['pizza-diameter__title']}>
                            Размер, см:
                        </Text>
                        <ul className={cn['pizza-diameter__variants']}>
                            <li className={cn['pizza-diameter__item']}>
                                <Button
                                    buttonType="button"
                                    customClass={clsx(
                                        cn['pizza-diameter__button'],
                                        pizzaIndex === 0 && cn['pizza-diameter__button_selected']
                                    )}
                                    onClick={updatePizza(0)}
                                >
                                    20
                                </Button>
                            </li>
                            <li className={cn['pizza-diameter__item']}>
                                <Button
                                    buttonType="button"
                                    customClass={clsx(
                                        cn['pizza-diameter__button'],
                                        pizzaIndex === 1 && cn['pizza-diameter__button_selected']
                                    )}
                                    onClick={updatePizza(1)}
                                >
                                    30
                                </Button>
                            </li>
                            <li className={cn['pizza-diameter__item']}>
                                <Button
                                    buttonType="button"
                                    customClass={clsx(
                                        cn['pizza-diameter__button'],
                                        pizzaIndex === 2 && cn['pizza-diameter__button_selected']
                                    )}
                                    onClick={updatePizza(2)}
                                >
                                    40
                                </Button>
                            </li>
                        </ul>
                    </div>

                    <PiazzaCardPurchase price={pizzaPrices[pizzaIndex]} onClick={console.log} />
                </div>
            )}
        </div>
    );
};

export default PizzaCard;
