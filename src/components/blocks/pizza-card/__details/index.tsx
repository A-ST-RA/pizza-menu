import React, { FC } from 'react';

import Text from '@/components/UI/Text';
import Title from '@/components/UI/Title';

import cn from '../style.module.sass';

interface IPizzaCardDetailsProps {
    title: string;
    description: string;
}

const PizzaCardDetails: FC<IPizzaCardDetailsProps> = ({ title, description }) => (
    <div className={cn['pizza-card__details']}>
        <Title customClass={cn['pizza-card__title']} title={title} level={4} />
        <Text customClass={cn['pizza-card__ingredients']} type="description">
            {description}
        </Text>
    </div>
);

export default PizzaCardDetails;
