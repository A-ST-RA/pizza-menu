import React, { FC } from 'react';

import Button from '@/components/UI/Button';
import Title from '@/components/UI/Title';

import cn from '../style.module.sass';

interface IPiazzaCardPurchaseProps {
    price: number;

    onClick: () => void;
}

const PiazzaCardPurchase: FC<IPiazzaCardPurchaseProps> = ({ price, onClick }) => (
    <div className={cn['pizza-card__purchases']}>
        <Title customClass={cn['pizza-card__price']} title={`${price}`} level={4} />
        <Button onClick={onClick} buttonType="button" customClass={cn['pizza-card__button']}>
            В корзину
        </Button>
    </div>
);

export default PiazzaCardPurchase;
