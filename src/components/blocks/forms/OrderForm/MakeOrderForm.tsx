/* eslint-disable @typescript-eslint/no-misused-promises */
import clsx from 'clsx';
import React, { FC, useState } from 'react';

import Button from '@/components/UI/Button';
import CustomInput from '@/components/UI/CustomInput';
import Modal from '@/components/UI/Modal';
import Radio from '@/components/UI/Radio';
import Text from '@/components/UI/Text';
import { PizzaMockData } from '@/data/catalog';

import PizzaCard from '../../pizza-card';
import cn from './style.module.sass';

interface IProps {
    isOpen: boolean;
    setIsOpen: (state: boolean) => void;
}

const MakeOrderForm: FC<IProps> = ({ isOpen, setIsOpen }): JSX.Element => {
    const [isChecked, setIsChecked] = useState<boolean>(true);

    return (
        <Modal isOpen={isOpen} onClose={setIsOpen}>
            <div className={cn.order}>
                <div className={cn['order__cards-list']}>
                    {PizzaMockData.slice(3).map(el => (
                        <PizzaCard key={el.id} {...el} isInCart />
                    ))}
                </div>
                <div className={clsx(cn['order__total-price'], cn.title)}>
                    Сумма заказа: <span>{2000}</span> руб
                </div>
                <form className={cn.order__form}>
                    <div className={clsx(cn.order__contacts, cn.order__title)}>Контакты</div>
                    <div className={cn['order__half-inputs']}>
                        <div className={clsx(cn.order__input)}>
                            <CustomInput name="name" placeholder="Ваше имя" />
                        </div>
                        <div className={cn.order__input}>
                            <CustomInput name="phone" placeholder="Телефон" />
                        </div>
                    </div>
                    <div className={clsx(cn.order__address)}>
                        <CustomInput placeholder="Адресс доставки" name="address" />
                    </div>
                    <div className={cn.order__title}>Способ оплаты</div>
                    <Radio
                        value="radio1"
                        name="radio1"
                        placeholder="Оплата наличными или картой курьеру"
                        onChange={() => setIsChecked(true)}
                        isChecked={isChecked}
                    />
                    <Radio
                        value="radio2"
                        name="radio2"
                        placeholder="Оплата картой онлайн на сайте"
                        onChange={() => setIsChecked(false)}
                        isChecked={!isChecked}
                    />
                    <Button buttonType="button" customClass={cn.order__confirmation}>
                        Оформить заказ
                    </Button>
                    <Text customClass={cn['order__confirmation-text']} type="description">
                        Нажимая кнопку «Оформить заказ» вы соглашаетесь с политикой
                        конфиденциальности
                    </Text>
                </form>
            </div>
        </Modal>
    );
};
export default MakeOrderForm;
