import clsx from 'clsx';
import React, { FC } from 'react';

import PizzaCard, { IPizzaCardProps } from '../pizza-card';
import cn from './style.module.sass';

interface IPIzzaListProps {
    pizzxcas: IPizzaCardProps[];
}

const PizzaList: FC<IPIzzaListProps> = ({ pizzxcas }) => (
    <div className={clsx(cn['pizza-list'], cn['pizza-list__wrapper'])}>
        {pizzxcas.map(el => (
            <div key={el.title} className={cn['pizza-list__element']}>
                <PizzaCard {...el} />
            </div>
        ))}
    </div>
);

export default PizzaList;
