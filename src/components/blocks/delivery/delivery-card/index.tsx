import Image from 'next/image';
import React, { FC } from 'react';

import Text from '@/components/UI/Text';
import Title from '@/components/UI/Title';

import cn from './style.module.sass';

interface IData {
    img: string;
    title: string;
    text: string;
}

const DeliveryCard: FC<IData> = ({ img, title, text }): JSX.Element => (
    <div className={cn['delivery-card']}>
        <div className={cn['delivery-card__content']}>
            <div className={cn['delivery-card__image']}>
                <Image width={75} height={72} src={img} alt={title} />
            </div>
            <div>
                <Title customClass={cn['delivery-card__title']} title={title} level={4} />
                <Text type="description">{text}</Text>
            </div>
        </div>
    </div>
);

export default DeliveryCard;
