import React from 'react';

import Title from '@/components/UI/Title';
import { IMockForNonPizzaCards } from '@/types/IMockForNonPizzaCards';

import DeliveryCard from './delivery-card';
import cn from './style.module.sass';

const mocks = [
    {
        id: 1,
        img: '/images/svg/info-cards/order.svg',
        title: 'Заказ',
        text: 'После оформления заказа мы свяжемся с вами для уточнения деталей.',
    },
    {
        id: 2,
        img: '/images/svg/info-cards/delivery.svg',
        title: 'Доставка курьером',
        text: 'Мы доставим вашу пиццу горячей. Бесплатная доставка по городу.',
    },
    {
        id: 3,
        img: '/images/svg/info-cards/pay.svg',
        title: 'Оплата',
        text: 'Оплатить можно наличными или картой курьеру. И золотом тоже можно.',
    },
];

const Delivery = () => (
    <div className={cn.delivery}>
        <div className={cn.delivery__container}>
            <Title customClass={cn.delivery__title} title="Доставка и оплата" level={2} />
            <div className={cn.delivery__grid}>
                {mocks.map((data: IMockForNonPizzaCards) => (
                    <DeliveryCard
                        key={data.id}
                        img={data.img}
                        text={data.text}
                        title={data.title}
                    />
                ))}
            </div>
        </div>
    </div>
);

export default Delivery;
