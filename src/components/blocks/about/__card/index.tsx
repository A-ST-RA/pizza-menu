import Image from 'next/image';
import React, { FC } from 'react';

import Text from '@/components/UI/Text';
import Title from '@/components/UI/Title';

import cn from './style.module.sass';

interface IData {
    img: string;
    title: string;
    text: string;
}

const AboutCard: FC<IData> = ({ img, text, title }): JSX.Element => (
    <div className={cn.about__card}>
        <div className={cn.about__image}>
            <Image width={304} height={304} src={img} alt={text} />
        </div>
        <div className={cn['about__card-content']}>
            <Title customClass={cn.about__title} title={title} level={3} />
            <Text type="description">{text}</Text>
        </div>
    </div>
);

export default AboutCard;
