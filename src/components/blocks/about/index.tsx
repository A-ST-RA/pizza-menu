import React from 'react';

import AboutCard from './__card';
import cn from './style.module.sass';

const data = [
    {
        id: 1,
        img: '/images/about/1.jpg',
        title: 'Изготавливаем пиццу по своим рецептам в лучших традициях',
        text:
            'Наша пицца получается сочной, вкусной и главное хрустящей с нежной и аппетитной начинкой, \n ' +
            'готовим по своим итальянским рецептам',
    },
    {
        id: 2,
        img: '/images/about/2.jpg',
        title: 'Используем только свежие ингридиенты',
        text: 'Ежедневно заготавливаем продукты и овощи для наших пицц, соблюдаем все сроки хранения',
    },
    {
        id: 3,
        img: '/images/about/3.jpg',
        title: 'Доставка в течение 60 минут или заказ за нас счёт',
        text:
            'Все наши курьеры – фанаты серии Need for Speed и призеры гонок World Rally Championship и \n' +
            ' World Superbike во всех категориях',
    },
];

const About = (): JSX.Element => (
    <div id="about" className={cn.about}>
        <div className={cn.about__wrapper}>
            <div className={cn.about__grid}>
                {data.map(
                    ({ id, ...rest }): JSX.Element => (
                        <AboutCard key={id} {...rest} />
                    )
                )}
            </div>
        </div>
    </div>
);

export default About;
