import React, { FC } from 'react';

import { LogoWhite } from '@/components/UI/svg/logo';

import Contacts from './__contacts';
import cn from './style.module.sass';

const Footer: FC = (): JSX.Element => (
    <footer className={cn.footer}>
        <div className={cn.footer__wrapper}>
            <div className={cn.footer__grid}>
                <div className={cn.footer__logo}>
                    <LogoWhite />
                </div>
                <div className={cn['footer__contacts-wrapper']}>
                    <Contacts />
                </div>
                <a className={cn.footer__rules} target="_blank" href="#" rel="license">
                    Политика конфиденциальности
                </a>
            </div>
        </div>
    </footer>
);

export default Footer;
