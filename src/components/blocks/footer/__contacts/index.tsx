import { clsx } from 'clsx';
import React from 'react';

import Button from '@/components/UI/Button';
import Text from '@/components/UI/Text';

import cn from './style.module.sass';

const FooterContacts = () => (
    <div className={clsx(cn.footer__contacts, cn.footer__contacts_wrapper)}>
        <Button buttonType="link" customClass={cn.footer__telephone} hrefLink="tel:+79184326587">
            +7 (918) 432-65-87
        </Button>
        <Text customClass={cn['footer__working-hours']} type="description">
            Ежедневно с 9:00 до 23:00
        </Text>
    </div>
);

export default FooterContacts;
