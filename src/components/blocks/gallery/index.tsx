import _ from 'lodash';
import Image from 'next/image';
import React from 'react';

import cn from './style.module.sass';

const amountOfImages = 11;

const images = _.range(1, amountOfImages).map(el => `${el}.jpg`);

const Gallery = () => (
    <div className={cn.gallery}>
        {images.map((img: string) => (
            <div key={img} className={cn.gallery__grid}>
                <Image src={`/images/gallery/${img}`} height={384} width={384} alt="image" />
            </div>
        ))}
    </div>
);

export default Gallery;
