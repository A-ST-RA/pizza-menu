import clsx from 'clsx';
import React, { FC } from 'react';

import cn from './style.module.sass';

interface IInputProps {
    name: string;
    placeholder: string;
    error?: string;
    rest?: { [key: string]: unknown };
}

const CustomInput: FC<IInputProps> = ({ error, placeholder, name, ...rest }) => (
    <div className={clsx(cn.input)}>
        <input placeholder={placeholder} className={cn.input__text} type="text" {...rest} />
        <label htmlFor={name} className={clsx(cn.input__label)}>
            {placeholder}
        </label>
        {error && <span className={cn.input__error}>{error}</span>}
    </div>
);

export default CustomInput;
