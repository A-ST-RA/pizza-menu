import clsx from 'clsx';
import React, { FC } from 'react';

import cn from './style.module.sass';

interface IProps {
    onToggle: (isStateMenu: boolean) => void;
    isStateMenu: boolean;
    color?: boolean;
}

const Burger: FC<IProps> = ({ onToggle, isStateMenu, color }: IProps) => (
    <div className={cn.wrapper}>
        <div role="presentation" className={cn.inner} onClick={() => onToggle(isStateMenu)}>
            <div className={clsx(isStateMenu ? cn.burgerClose : cn.burger, color && cn.color)} />
        </div>
    </div>
);

export default Burger;
