import { All } from './all';
import { Cheese } from './cheese';
import { Hot } from './hot';
import { Meat } from './meat';
import { Vegan } from './vegan';

export const PizzaIconMatcher = {
    0: All,
    1: Hot,
    2: Meat,
    3: Cheese,
    4: Vegan,
};
