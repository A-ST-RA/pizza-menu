import clsx from 'clsx';
import React from 'react';

import cn from './style.module.sass';

export interface ITitle {
    title: string;
    // eslint-disable-next-line no-magic-numbers
    level: 1 | 2 | 3 | 4;
    customClass?: string;
}

type HeadingTag = 'h1' | 'h2' | 'h3' | 'h4';

const Title: React.FC<ITitle> = ({ title, level = 1, customClass }) => {
    const Tag = `h${level}` as HeadingTag;
    return (
        <Tag
            className={clsx(cn[`h${level}`], customClass)}
            dangerouslySetInnerHTML={{ __html: title }}
        />
    );
};

export default Title;
