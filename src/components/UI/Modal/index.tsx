import clsx from 'clsx';
import React, { FC, ReactNode } from 'react';

import Burger from '../Burger';
import cn from './style.module.sass';

interface IProps {
    children: ReactNode;
    isOpen: boolean;
    onClose: (state: boolean) => void;
}

const Modal: FC<IProps> = ({ children, onClose, isOpen }) => (
    <div className={clsx(cn.modal, isOpen && cn.modal_open)}>
        <div className={cn.modal__box}>
            <div className={cn.modal__header}>
                <div>Ваш заказ</div>
                <div>
                    <Burger isStateMenu onToggle={() => onClose(false)} color />
                </div>
            </div>
            <div>{children}</div>
        </div>
    </div>
);

export default Modal;
