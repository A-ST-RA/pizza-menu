import clsx from 'clsx';
import React, { FC } from 'react';

import { InvertLogo } from '../svg/logo/invert';
import cn from './style.module.sass';

interface IProps {
    customClass?: string;
}

const Logo: FC<IProps> = ({ customClass }) => (
    <div className={cn['header-logo']}>
        <InvertLogo className={clsx(cn['header-logo__image'], customClass)} />
    </div>
);

export default Logo;
