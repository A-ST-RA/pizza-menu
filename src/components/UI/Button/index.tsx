/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/button-has-type */
/* eslint-disable react/react-in-jsx-scope */
import clsx from 'clsx';
import { ButtonHTMLAttributes, DetailedHTMLProps, FC, ReactNode } from 'react';

import cn from './style.module.sass';

interface ButtonProps
    extends DetailedHTMLProps<ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement> {
    children: ReactNode;
    buttonType?: 'button' | 'link';
    hrefLink?: string;
    customClass?: string;
    onClick?: () => void;
}

const Button: FC<ButtonProps> = ({
    hrefLink,
    children,
    onClick,
    customClass,
    buttonType = 'button',
    ...props
}) => {
    const matchTypes = {
        button: (
            <button
                type="button"
                className={clsx(cn.button, customClass)}
                onClick={onClick}
                {...props}
            >
                {children}
            </button>
        ),
        link: (
            <a href={hrefLink} className={clsx(cn.button, cn.button__link, customClass)}>
                {children}
            </a>
        ),
    };

    return matchTypes[buttonType];
};

export default Button;
