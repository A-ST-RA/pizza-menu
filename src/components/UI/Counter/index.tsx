import React, { FC } from 'react';

import Decrement from '../svg/counter/decrement';
import Increment from '../svg/counter/increment';
import cn from './style.module.sass';

interface CounterProps {
    inc: () => void;
    dec: () => void;
    count: number;
}

const Counter: FC<CounterProps> = ({ inc, dec, count }): JSX.Element => (
    <div className={cn.counter}>
        <Decrement onClick={dec} />
        <input type="number" value={count} />
        <Increment onClick={inc} />
    </div>
);

export default Counter;
