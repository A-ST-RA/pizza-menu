/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable no-magic-numbers */
import { clsx } from 'clsx';
import React from 'react';

import cn from './style.module.sass';

interface TextProps {
    type: 'accent' | 'description' | 'paragraph';
    children?: string;
    customClass?: string;
    onClick?: () => void;
}

const Text: React.FC<TextProps> = ({ type, children, customClass, onClick }) => (
    <p className={clsx(cn[type], customClass)} onClick={onClick}>
        {children}
    </p>
);

export default Text;
