import React, { FC } from 'react';

import cn from './style.module.sass';

interface IRadioProps {
    isChecked?: boolean;
    onChange?: () => void;
    value: string;
    name: string;
    placeholder: string;
}

const Radio: FC<IRadioProps> = ({ isChecked, onChange, value, name, placeholder }) => (
    <div className={cn.radio}>
        <input
            type="radio"
            className={cn.radio__element}
            id={name}
            value={value}
            checked={isChecked}
            onChange={onChange}
        />
        <label htmlFor={name} className={cn.radio__label}>
            {placeholder}
        </label>
    </div>
);

export default Radio;
