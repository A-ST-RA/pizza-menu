import Head from 'next/head';
import React, { useMemo, useState } from 'react';

import About from '@/components/blocks/about';
import Banner from '@/components/blocks/banner/Banner';
import Delivery from '@/components/blocks/delivery';
import FindUs from '@/components/blocks/find-us';
import Footer from '@/components/blocks/footer';
import Gallery from '@/components/blocks/gallery';
import Header from '@/components/blocks/header';
import { PizzaFilter } from '@/components/blocks/pizza-filter';
import PizzaList from '@/components/blocks/pizza-list';
import SuggestionList from '@/components/blocks/suggestion-list';
import { PizzaMockData } from '@/data/catalog';
import { indexOfChoose } from '@/types/pizzaTagIdx';

function IndexPage() {
    const [activePizzaTag, setAcrivePizzaTag] = useState<indexOfChoose>(0);
    const pizzaList = useMemo(() => {
        if (activePizzaTag === 0) return PizzaMockData;
        return PizzaMockData.filter(el => el.sort === activePizzaTag);
    }, [activePizzaTag]);

    return (
        <>
            <Head>
                <title>ПИЦЦЫ! ПИЦЦЫ!! ПИЦЦЫ!!!</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <Header />
            <Banner />
            <SuggestionList />
            <PizzaFilter
                tags={['', '', '', '', '']}
                activeId={activePizzaTag}
                setActive={(idx: indexOfChoose) => setAcrivePizzaTag(idx as indexOfChoose)}
            />
            <PizzaList pizzxcas={pizzaList} />
            <Delivery />
            <About />
            <FindUs />
            <Gallery />
            <Footer />
        </>
    );
}

export default IndexPage;
