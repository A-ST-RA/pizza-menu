export interface IMockForNonPizzaCards {
    id: number;
    img: string;
    title: string;
    text: string;
}
